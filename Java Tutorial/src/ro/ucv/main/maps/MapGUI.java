package ro.ucv.main.maps;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import org.jdesktop.swingx.JXMapKit;
import org.jdesktop.swingx.JXMapViewer;
import org.jdesktop.swingx.mapviewer.GeoPosition;
import org.jdesktop.swingx.mapviewer.Waypoint;
import org.jdesktop.swingx.mapviewer.WaypointPainter;
import org.jdesktop.swingx.mapviewer.WaypointRenderer;

import ro.ucv.main.maps.models.Area;
import ro.ucv.main.maps.models.MPoint;

public class MapGUI extends JFrame implements MouseListener {

	private static final int RANDOM_POINTS_NUMBER = 8;
	private int noPointsMap = 0;

	private JTextField longitude = new JTextField();
	private JTextField latitude = new JTextField();
	private JTextField radius = new JTextField();
	private JTextField noPointsArea = new JTextField();

	private JTextArea areaPoints = new JTextArea();
	private JScrollPane pointsPane = new JScrollPane(areaPoints, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
			ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

	private int ovalRadius = 0;
	private Double lastZoom;
	private Point lastClick = null;
	private GeoPosition lastClickedLocation;
	private WaypointPainter waypointPainter = new WaypointPainter();
	private JXMapKit map;

	public MapGUI() {
		super("Open Map - needs internet connection");

		this.setVisible(true);
		this.setBounds(150, 150, 850, 850);
		this.setLayout(null);

		map = (JXMapKit) (new SetupMap()).createOpenMap();
		map.setBounds(20, 20, 600, 300);
		map.getMainMap().addMouseListener(this);
		add(map);

		JLabel lblLong = new JLabel("Longitude");
		JLabel lblLat = new JLabel("Latitude");
		JLabel lblRadius = new JLabel("Radius");
		JLabel lblareaPoints = new JLabel("Area points");
		lblLong.setBounds(20, 330, 150, 25);
		lblLat.setBounds(180, 330, 150, 25);
		lblRadius.setBounds(340, 330, 150, 25);
		lblareaPoints.setBounds(20, 400, 150, 25);
		add(lblLong);
		add(lblLat);
		add(lblRadius);
		add(lblareaPoints);

		longitude.setBounds(20, 365, 150, 25);
		latitude.setBounds(180, 365, 150, 25);
		radius.setBounds(340, 365, 150, 25);
		noPointsArea.setBounds(120, 400, 70, 25);
		pointsPane.setBounds(20, 430, 300, 100);
		add(longitude);
		add(latitude);
		add(radius);
		add(noPointsArea);
		add(pointsPane);
		// Add a sample waypoint.

		waypointPainter.setRenderer(new WaypointRenderer() {

			public boolean paintWaypoint(Graphics2D g, JXMapViewer map, Waypoint wp) {
				boolean isMyClick = wp.getPosition().equals(lastClickedLocation);
				float alpha = 0.6f;
				int type = AlphaComposite.SRC_OVER;
				AlphaComposite composite = AlphaComposite.getInstance(type, alpha);
				Color color = isMyClick ? new Color(1, 0, 0, alpha) : new Color(0, 0, 1, alpha); // Red

				System.out.println("paint: " + wp.getPosition());
				g.setColor(color);
				g.fillOval(-5, -5, 10, 10);
				g.setColor(Color.BLACK);
				g.drawOval(-5, -5, 10, 10);
				g.setColor(Color.BLACK);
				MPoint mp = (MPoint) wp;
				g.drawString(mp.getLabel(), 0, -10);

				if (ovalRadius > 0 && isMyClick) {

					Double currentZoom = lastClick.distance(map.getCenter());
					Double r = currentZoom / lastZoom * ovalRadius;
					int zoomedRadius = r.intValue();
					System.out.println("radius " + ovalRadius + "   zoomed" + zoomedRadius);
					g.setColor(color);
					g.fillOval(-zoomedRadius, -zoomedRadius, 2 * zoomedRadius, 2 * zoomedRadius);
					g.setColor(Color.RED);
					g.drawOval(-zoomedRadius, -zoomedRadius, 2 * zoomedRadius, 2 * zoomedRadius);

				}
				return true;
			}
		});

		generateRandomPoints();
		repaint();
	}

	private Area getAreaPoints() {
		final Area area = new Area(lastClickedLocation, Double.valueOf(radius.getText()));
		final List<MPoint> areaPoints = new ArrayList();
		Set<MPoint> waypoints = waypointPainter.getWaypoints();

		for (MPoint point : waypoints) {
			if (geoDistance(lastClickedLocation, point.getPosition()) <= area.getRadius()) {
				areaPoints.add(point);
			}
		}

		area.setPoints(areaPoints);
		return area;
	}

	private void displayAreaPoints() {
		Area area = getAreaPoints();
		List<MPoint> mPoints = area.getPoints();
		StringBuilder builder = new StringBuilder();

		for (MPoint point : mPoints) {
			builder.append(point.getLongitude()).append(" ").append(point.getLatitude()).append(" ")
					.append(point.getLabel() + "\n");
		}

		areaPoints.setText(builder.toString());
		noPointsArea.setText(String.valueOf(mPoints.size()));
	}

	/**
	 * this draws on the map at clicked location .
	 *
	 * @param g
	 */
	public void moveWaypoint(GeoPosition g) {

		// Modify this if you need to add a set of markers instead of a single
		// one.
		waypointPainter.getWaypoints().add(new MPoint(g.getLatitude(), g.getLongitude(), "P" + noPointsMap++));
		map.getMainMap().setOverlayPainter(waypointPainter);

	}

	public double geoDistance(GeoPosition g1, GeoPosition g2) {
		final int EARTHRADIUS = 6371; // The radius of the earth in kilometers

		// Get the distance between latitudes and longitudes
		double deltaLat = Math.toRadians(g1.getLatitude() - g2.getLatitude());
		double deltaLong = Math.toRadians(g1.getLongitude() - g2.getLongitude());

		// Apply the Haversine function
		double a = Math.sin(deltaLat / 2) * Math.sin(deltaLat / 2) + Math.cos(Math.toRadians(g2.getLatitude()))
				* Math.cos(Math.toRadians(g1.getLatitude())) * Math.sin(deltaLong / 2) * Math.sin(deltaLong / 2);
		return EARTHRADIUS * 2 * Math.asin(Math.sqrt(a));
	}

	private void generateRandomPoints() {
		GeoPosition centerGeoPosition = map.getMainMap().getCenterPosition();
		System.out.println("center geo position" + centerGeoPosition);
		MPoint newPoint = null;
		Random random = new Random();

		for (int i = 0; i < RANDOM_POINTS_NUMBER; i++) {
			if (i % 2 == 0) {
				newPoint = new MPoint(centerGeoPosition.getLatitude() + random.nextDouble() / 10,
						centerGeoPosition.getLongitude() + random.nextDouble() / 10, "P" + i);
			} else {
				newPoint = new MPoint(centerGeoPosition.getLatitude() - random.nextDouble() / 10,
						centerGeoPosition.getLongitude() - random.nextDouble() / 10, "P" + i);

			}

			waypointPainter.getWaypoints().add(newPoint);
		}
		noPointsMap = RANDOM_POINTS_NUMBER + 1;
		map.getMainMap().setOverlayPainter(waypointPainter);
	}

	public void mouseClicked(MouseEvent e) {
		JXMapViewer m = map.getMainMap();
		GeoPosition g = m.convertPointToGeoPosition(e.getPoint());

		System.out.println("geo position click:  " + g);
		latitude.setText("" + g.getLatitude());
		longitude.setText("" + g.getLongitude());

		if (e.getButton() == MouseEvent.BUTTON1) {

			lastClick = e.getPoint();
			lastClickedLocation = g;
			ovalRadius = 0;
			areaPoints.setText("");
			noPointsArea.setText("");
			moveWaypoint(g);

		} else {
			if (lastClick != null) {
				radius.setText("" + geoDistance(lastClickedLocation, g));
				ovalRadius = ((Double) (lastClick.distance(e.getPoint()))).intValue();
				lastZoom = lastClick.distance(map.getMainMap().getCenter());
				displayAreaPoints();
			} else
				moveWaypoint(g);

		}

		repaint();
	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}
}
