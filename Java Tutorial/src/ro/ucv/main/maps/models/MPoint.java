package ro.ucv.main.maps.models;

import org.jdesktop.swingx.mapviewer.Waypoint;

/**
 * 
 * @author Nicoleta
 *
 */
public class MPoint extends Waypoint{

	private String label;

	public MPoint(double latitude, double longitude, String label) {
		super(latitude, longitude);
		this.label = label;
	}

	public double getLatitude() {
		return super.getPosition().getLatitude();
	}

	public double getLongitude() {
		return super.getPosition().getLongitude();
	}

	public String getLabel() {
		return label;
	}
	
}
