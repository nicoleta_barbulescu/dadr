package ro.ucv.main.maps.models;

import java.util.List;

import org.jdesktop.swingx.mapviewer.GeoPosition;

/**
 * 
 * @author Nicoleta
 *
 */
public class Area {

	private GeoPosition center;
	private Double radius;
	private List<MPoint> points;

	public Area(GeoPosition center, Double radius) {
		this.center = center;
		this.radius = radius;
	}

	public GeoPosition getCenter() {
		return center;
	}

	public void setCenter(GeoPosition center) {
		this.center = center;
	}

	public Double getRadius() {
		return radius;
	}

	public void setRadius(Double radius) {
		this.radius = radius;
	}

	public List<MPoint> getPoints() {
		return points;
	}

	public void setPoints(List<MPoint> points) {
		this.points = points;
	}

}
